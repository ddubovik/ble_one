﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NFPCommunication
{
    public class NFPResponseError : NFPResponse
    {

        private string ErrorMessage;
        public NFPResponseError(byte[] responsePacket) : base(NFPResponseCode.PRV_ANS_NOK, responsePacket)
        {

        }

        protected override void ParseParameters()
        {
            ErrorMessage = ResponseCodeDescription.GetResponseCodeDescription((NFPResponseCode)_parameters[0]);

        }


        public override string ToString()
        {
            if (this.IsPacketValid)
            {
                return $"Response Code: Error, Message: {ErrorMessage}";
            }
            else
            {
                return $"Error: {this.PacketValidationMessage}";
            }
        }

    }
}
