﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NFPCommunication
{
    public static class NFPResponseFactory
    {
        /*
        private static readonly Dictionary<NFPResponseCode, NFPResponse> _responses = new Dictionary<NFPResponseCode, NFPResponse>
        {
                { NFPCommandCode.PRB_KEEP_ALV,  new NFPCommand(NFPCommandCode.PRB_KEEP_ALV, null) },
                { NFPCommandCode.PRB_READ_2W,   new NFPCommand(NFPCommandCode.PRB_READ_2W,  new byte[] { 0x4E, 0x20, 0x4E, 0x20, 0x22, 0x02, 0x03 })},
                { NFPCommandCode.PRB_READ_3W,   new NFPCommand(NFPCommandCode.PRB_READ_3W,  new byte[] { 0x4E, 0x20, 0x4E, 0x20, 0x22, 0x02, 0x03 })},
                { NFPCommandCode.PRB_QRY_2W,    new NFPCommand(NFPCommandCode.PRB_QRY_2W,   new byte[] { 0x4E, 0x20, 0x4E, 0x20, 0x22, 0x02, 0x03 })}
        };
        public static NFPResponse GetResponse(byte [] responsePacket)
        {
            return _commands[ComandCode];
        }
        */

        public static NFPResponse GetResponse(NFPCommandCode commandCode,  byte[] responsePacket)
        {
            NFPResponseCode responseCode = (NFPResponseCode)responsePacket[2];

            NFPResponse response;

            if (responseCode == NFPResponseCode.PRB_ANS_OK)
            {
                switch (commandCode)
                {
                    case NFPCommandCode.PRB_KEEP_ALV:
                        response = new NFPResponseKeepAliveOK(responsePacket);
                        break;
                    case NFPCommandCode.PRB_READ_2W:
                        response = new NFPResponseRead2WOK(responsePacket);
                        break;
                    case NFPCommandCode.PRB_READ_3W:
                        response = new NFPResponseRead2WOK(responsePacket);
                        break;
                    case NFPCommandCode.PRB_QRY_2W:
                        response = new NFPResponseQuery2WOK(responsePacket);
                        break;
                    case NFPCommandCode.PRB_PGM_2W:
                        response = new NFPResponseQuery2WOK(responsePacket);
                        break;
                    case NFPCommandCode.PRB_NWREAD_2W:
                    case NFPCommandCode.PRB_NWREAD_3W:
                        response = new NFPResponseRead2WOK(responsePacket);
                        break;
                    default:
                        response = new NFPResponseError(responsePacket);
                        break;
                }
            }
            else
            {
                response = new NFPResponseError(responsePacket);
            }

            return response;
        }
    }
}
