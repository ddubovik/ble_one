﻿using System;

namespace NFPCommunication
{
    public class NFPCharacteristic
    {
        public string Name { get; private set; }
        public NFPCharacteristicIdentifier Id { get; private set; }
        public Guid UUID { get; private set; }

        public NFPCharacteristic(NFPCharacteristicIdentifier id, string name, Guid uuid)
        {
            Name = name;
            Id = id;
            UUID = uuid;
        }
    }
}
