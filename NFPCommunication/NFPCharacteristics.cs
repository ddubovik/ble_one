﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace NFPCommunication
{
    public static class NFPCharacteristics
    {
        private static readonly IList<NFPCharacteristic> _characteristics = new List<NFPCharacteristic>()
        {

            new NFPCharacteristic(NFPCharacteristicIdentifier.DeviceName, "Device Name", new Guid("00002a00-0000-1000-8000-00805f9b34fb")),
            new NFPCharacteristic(NFPCharacteristicIdentifier.BLEAppearance, "BLE Appearance", new Guid("00002a01-0000-1000-8000-00805f9b34fb")),
            new NFPCharacteristic(NFPCharacteristicIdentifier.BLEPeripheralPreferredConnectionParameters, "BLE Peripheral Preferred Connection Parameters", new Guid("00002a04-0000-1000-8000-00805f9b34fb")),
            new NFPCharacteristic(NFPCharacteristicIdentifier.RX, "RX UART", new Guid("4E455054-554E-4554-4749-4E5447490001")),
            new NFPCharacteristic(NFPCharacteristicIdentifier.TX, "TX UART", new Guid("4E455054-554E-4554-4749-4E5447490002")),
            new NFPCharacteristic(NFPCharacteristicIdentifier.ErrorsAndAlerts, "Errors And Alerts", new Guid("4E455054-554E-4554-4749-4E5447490002")),
            new NFPCharacteristic(NFPCharacteristicIdentifier.BatteryStateOfCharge, "Battery State Of Charge", new Guid("00002a19-0000-1000-8000-00805f9b34fb"))

        };

        public static NFPCharacteristic GetServiceById(NFPCharacteristicIdentifier id)
        {
            return _characteristics.FirstOrDefault(s => s.Id == id);
        }
    }
}
