﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NFPCommunication
{
    public static class NFPCommandFactory
    {
        /*
        private static readonly Dictionary<NFPCommandCode, NFPCommand> _commands = new Dictionary<NFPCommandCode, NFPCommand>
        {
                { NFPCommandCode.PRB_KEEP_ALV,  new NFPCommand(NFPCommandCode.PRB_KEEP_ALV, null) },
                { NFPCommandCode.PRB_READ_2W,   new NFPCommand(NFPCommandCode.PRB_READ_2W,  new byte[] { 0x4E, 0x20, 0x4E, 0x20, 0x22, 0x02, 0x03 })},
                { NFPCommandCode.PRB_READ_3W,   new NFPCommand(NFPCommandCode.PRB_READ_3W,  new byte[] { 0x4E, 0x20, 0x4E, 0x20, 0x22, 0x02, 0x03 })},
                { NFPCommandCode.PRB_QRY_2W,    new NFPCommand(NFPCommandCode.PRB_QRY_2W,   new byte[] { 0x4E, 0x20, 0x4E, 0x20, 0x22, 0x02, 0x03 })},
                { NFPCommandCode.PRB_PGM_2W,    new NFPCommand(NFPCommandCode.PRB_PGM_2W,   new byte[] { 0x4E, 0x20, 0x4E, 0x20, 0x22, 0x02, 0x03,
                                  0x82, 0xB1, 0x30, 0x30, 0x53, 0xD7, 0x17, 0x7B, 0x17, 0xB1, 0xB2, 0x33, 0xB4, 0x35, 0x36, 0xB7, 0xB8, 0x39, 0x30,
                                  0x17, 0xA0, 0x17, 0xCA, 0xD7, 0xCC, 0x17, 0xFC, 0x03, 0x00, 0x20, 0x56, 0x48})}
        };



        public static NFPCommand GetCommand(NFPCommandCode ComandCode, )
        {
            return _commands[ComandCode];
        }
        */
        public static NFPCommand GetCommand(NFPCommandCode functionCode,  byte[] programData, short? network)
        {
            NFPCommand command;

            NFPCommandParameters param = new NFPCommandParameters();

            switch (functionCode)
            {
                case NFPCommandCode.PRB_KEEP_ALV:
                    command = new NFPCommand(functionCode, null, null);
                    break;
                case NFPCommandCode.PRB_READ_2W:
                    command = new NFPCommand(functionCode, param.GetCommandParameters(10, 6, null), programData);
                    break;
                case NFPCommandCode.PRB_READ_3W:
                    command = new NFPCommand(functionCode,param.GetCommandParameters(10, 6, null),programData);
                    break;
                case NFPCommandCode.PRB_QRY_2W:
                    command = new NFPCommand(functionCode, param.GetCommandParameters(10, 6, null), programData);
                    break;
                case NFPCommandCode.PRB_NWREAD_2W:
                case NFPCommandCode.PRB_NWREAD_3W:
                    //byte[] pm = new byte[param.GetCommandParameters(6).Length + 1];
                    // param.GetCommandParameters().CopyTo(pm, 0);
                    //// pm[param.GetCommandParameters().Length + 1] = 0x7F;
                    // pm[param.GetCommandParameters().Length] = (byte)network;

                    ////byte[] pm = { 0x07, 0xD0, 0x07, 0xD0, 0x22, 0x02, 0x03, 0x02 };
                    ////byte[] pm = { 0x07, 0xD0, 0x07, 0xD0, 0x22, 0x02, 0x03 };
                    //command = new NFPCommand(functionCode, pm, programData);
                    command = new NFPCommand(functionCode, param.GetCommandParameters(10, 6, network), programData);
                    break;
                default:
                    command = new NFPCommand(functionCode, param.GetCommandParameters(10, 6, null), programData);
                    /*command = new NFPCommand(NFPCommandCode.PRB_PGM_2W, new byte[] { 0x4E, 0x20, 0x4E, 0x20, 0x22, 0x02, 0x03 },
                                  new byte[] {0x82, 0xB1, 0x30, 0x30, 0x53, 0xD7, 0x17, 0x7B, 0x17, 0xB1, 0xB2, 0x33, 0xB4, 0x35, 0x36, 0xB7, 0xB8, 0x39, 0x30,
                                  0x17, 0xA0, 0x17, 0xCA, 0xD7, 0xCC, 0x17, 0xFC, 0x03, 0x00, 0x20, 0x56, 0x48});*/
                    break;
            }

            return command;
        }
        


    }
}
