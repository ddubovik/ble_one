﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NFPCommunication
{
    public class NFPResponseKeepAliveOK : NFPResponse
    {

        public int MajorVersion { get; protected set; }

        public int MinorVersion { get; protected set; }


        public NFPResponseKeepAliveOK(byte[] responsePacket) : base (NFPResponseCode.PRB_ANS_OK, responsePacket)
        {
            int c = 7;
        }


        protected override void ParseParameters()
        {
            MinorVersion = _parameters[0]&0x0F;
            MajorVersion = (_parameters[0]&0xF0) >> 4;
        }


        public override string ToString()
        {
            if (this.IsPacketValid)
            {
                return $"Response Code: OK, Version: {MajorVersion}.{MinorVersion}";
            }
            else
            {
                return $"Error: {this.PacketValidationMessage}";
            }
        }
    }
}
