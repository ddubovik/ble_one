﻿using NFPCommunication.Extentions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NFPCommunication
{
    public class NFPProgramData
    {
        private byte _stx = 0x02;
        private byte _dataFormat = 0xB1;
        private short _networkID = 0;
        private byte _network1 = 0x30;
        private byte _network0 = 0x30;
        private byte _manufacturer = 0x4E;  //N
        //private byte _manufacturer = 0x53;  
        private byte _meterType = 0x57;     //W
        private byte _etb = 0x17;
        private byte _leftCurlyBracket = 0x7B;
        private long _registerNumber = 0;
        private byte[] _registerNumberBytes;// = new byte[10];
        private byte[] _fillInNulls;
        private byte _rightCurlyBracket = 0x7D;
        //private byte _rightCurlyBracket = 0xA0;
        private byte _custChar0 = 0x4E;     //N
        private byte _custChar1 = 0x45;     //E
        private byte _custChar2 = 0x50;     //P
        //private byte _user0 = 0xCA;     
        //private byte _user1 = 0xD7;     
        //private byte _user2 = 0xCC;
        private byte _checksumToken = 0xFC;
        private byte _etx = 0x03;
        private byte _netId = 0x00;
        private byte _modeFlags = 0x20; // 
        private byte _readFlags;// = 0x56; //0101 0110  - 0/5 resolution, 6 digits default
                                        //private byte _readFlags = 0x58;  //0101 1000 - 0/5 resolution, 8 digits
                                        // private byte _readFlags = 0x98;  //1001 1000 ??

        //private List<byte> _programmData;
        //public int NumberIdDigits 
        public int MeterNumberDigits { get; set; } =  10;


        public NFPProgramData()
        {
            //CustChar0 = ' ';
            //CustChar1 = ' ';
            //CustChar1 = ' ';
        }

        public String DialCode { get; set; }

        private NFPDataFormat DataFormat { get; set; } = NFPDataFormat.BASIC;
      

        public long RegisterNumber
        {
            get
            {
                return _registerNumber;
            }
            set
            {
                if (value >= 0 && value <= 9999999999)
                {
                    _registerNumber = value;
                }
                else
                {
                    _registerNumber = 0;
                    throw new ArgumentOutOfRangeException("MeterNumber", "Meter Number has to be between 0 and 9999999999");
                }
            }
        }

        //TODO check validation
        public short NetworkNumber
        {
            get
            {
                return _networkID;
            }
            set
            {
                if ((value >= 0 && value <=89) || value == 99)
                {
                    _networkID = value;
                }
                else
                {
                    _networkID = 0;
                    throw new ArgumentOutOfRangeException("NetworkNumber", "Network Number has to be between 0 and 89 it also can be 99 for network master.");
                }
            }
        }

        public short NetworkSize { get; set; }

        public short StopBits { get; set; } // 2,  1

        public short ClockDivisor { get; set; } // 1, 16

        public string WireType { get; set; } //2W, 3W

        public short ProReadMode { get; set; } // 0 - defailt or "Standart", 1 - "ProRead"

        public char CustChar0
        {
            get
            {
                return Encoding.ASCII.GetChars(new byte[] { _custChar0 })[0];
            }
            set
            {
                _custChar0 = Encoding.ASCII.GetBytes(new char[] { value })[0];
            }
        }


        public char CustChar1
        {
            get
            {
                return Encoding.ASCII.GetChars(new byte[] { _custChar1 })[0];
            }
            set
            {
                _custChar1 = Encoding.ASCII.GetBytes(new char[] { value })[0];
            }
        }
        public char CustChar2
        {
            get
            {
                return Encoding.ASCII.GetChars(new byte[] { _custChar2 })[0];
            }
            set
            {
                _custChar2 = Encoding.ASCII.GetBytes(new char[] { value })[0];
            }
        }

        private void BuildNetworkID()
        {
            byte[] nb = new byte[2];
            if (NetworkNumber == 99)
            {
                nb = Encoding.ASCII.GetBytes(NetworkSize.ToString());
            }
            else
            {
                nb = Encoding.ASCII.GetBytes(NetworkNumber.ToString());
            }
        
            _network0 = nb[0];
            if (nb.Length == 2)
            {
                _network1 = nb[1];
            }
            else
            {
                _network1 = 0x30;
            }

            _netId = (byte)((NetworkNumber/10)* 16 + NetworkNumber%10);
        }

        private void BuildDataFormat()
        {
            _dataFormat = (byte)DataFormat;
        }
        private void BuildMeterNumberBytes()
        {
            byte[] bytesFromString = Encoding.ASCII.GetBytes(RegisterNumber.ToString()).Reverse().ToArray();
            _registerNumberBytes = new byte[MeterNumberDigits];
            for (int i = 0; i < MeterNumberDigits; i++)
            {
                if (i < bytesFromString.Length)
                {
                    _registerNumberBytes[i] = bytesFromString[i];
                }
                else
                {
                    _registerNumberBytes[i] = Encoding.ASCII.GetBytes("0")[0].BuildParity();
                }
            }

            if (MeterNumberDigits < 10)
            {
                _fillInNulls = new byte[10 - MeterNumberDigits];
                for(int i = 0; i<_fillInNulls.Length; i++)
                {
                    _fillInNulls[i] = 0;
                }
            }
            /*
            for (int i= 0; i < _meterNumberBytes.Length; i++)
            {
                if (i < bytesFromString.Length)
                {
                    _meterNumberBytes[i] = bytesFromString[i];
                }
                else
                {
                    _meterNumberBytes[i] = Encoding.ASCII.GetBytes("0")[0].BuildParity();
                }
            }
            */
        }

        private byte BuildCheckSum(byte[] data)
        {
            int result = 0;
            foreach (byte param in data)
            {
                result += (int)param;
            }

            return (byte)((result - 123) & 0xFF);
        }

        private void BuildReadFlags()
        {
            if (DialCode.Length !=2)
            {
                throw new Exception("Dial Code has invalid value.");
            }
            byte b1 = Convert.ToByte(DialCode[1]);
            byte b2 = Convert.ToByte(DialCode[0]);

            _readFlags = Convert.ToByte(((b1 - Convert.ToByte('0')) << 4) | (b2 - Convert.ToByte('0')));
        }

        private void BuildModeFlags()
        {
            _modeFlags = 0x20;
            if (WireType == "3W")
            {
                _modeFlags = 0x30;
            }

            if (StopBits == 1)
            {
                _modeFlags |= 0x40; //See ETI 16-02
            }
            if (ClockDivisor == 1)
            {
                _modeFlags |= 0x80;
            }

            if (ProReadMode == 1)
            {
                _modeFlags |= 0x04;
            }
        }

        public byte[] BiuldProgramData()
        {
            BuildMeterNumberBytes();

            BuildNetworkID();

            BuildDataFormat();

            BuildReadFlags();

            BuildModeFlags();

            List<byte> _programmData = new List<byte>();
            _programmData.Add(_stx.BuildParity());
            _programmData.Add(_dataFormat);
            _programmData.Add(_network1.BuildParity());
            _programmData.Add(_network0.BuildParity());
            _programmData.Add(_manufacturer.BuildParity());
            _programmData.Add(_meterType.BuildParity());
            _programmData.Add(_etb.BuildParity());
            _programmData.Add(_leftCurlyBracket.BuildParity());
            _programmData.Add(_etb.BuildParity());
            _programmData.AddRange(_registerNumberBytes.Select(b => b.BuildParity()).Reverse().ToList());
            _programmData.Add(_etb.BuildParity());
            _programmData.Add(_rightCurlyBracket.BuildParity());
            _programmData.Add(_etb.BuildParity());
            _programmData.Add(_custChar0.BuildParity());
            _programmData.Add(_custChar1.BuildParity());
            _programmData.Add(_custChar2.BuildParity());
            _programmData.Add(_etb.BuildParity());
            _programmData.Add(_checksumToken.BuildParity());
            _programmData.Add(_etx.BuildParity());
            if (MeterNumberDigits < 10)
            {
                _programmData.AddRange(_fillInNulls.ToList());
            }
            // _programmData.Add(_netId.BuildParity()); TODO: Check if we need to build a parity
            _programmData.Add(_netId);
            //_programmData.Add(_modeFlags.BuildParity());
            //_programmData.Add(_readFlags.BuildParity());

            _programmData.Add(_modeFlags);
            
            _programmData.Add(_readFlags);

            byte checkSum = BuildCheckSum(_programmData.Skip(1).Take(25).ToArray());
            //byte checkSum = 0x35;
            _programmData.Add(checkSum);

            return _programmData.ToArray();

        }



    }
}
