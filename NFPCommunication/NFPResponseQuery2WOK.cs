﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFPCommunication.Extentions;
namespace NFPCommunication
{
    public class NFPResponseQuery2WOK : NFPResponse
    {

        private byte _formatCode;
        public String FormatCode
        {
            get
            {
                if (_formatCode == 0xB1)
                {
                    return "6-Digit";
                }
                if (_formatCode == 0xB2)
                {
                    return "8-Didit";
                }
                return "Unknown";
            }
        }

        private byte _networkId1;
        private byte _networkId0;

        private byte _netId;

        private byte _manufacturerId;

        private byte _registerType;

        private byte[] _registerNumber;

        private byte _packetCheckSum;

        private byte _readFlags;

        private byte _modeFlags;

        private byte _custChar0;
        private byte _custChar1;
        private byte _custChar2;


        public string NetworkId => (new byte[] { _networkId1, _networkId0 }).GetStringFromParity();

        public String ManufacturerId => _manufacturerId.GetStringFromParity();

        public String RegisterType => _registerType.GetStringFromParity();


        public string RegisterNumber => _registerNumber.GetStringFromParity();


        public short NetworkNumber => Convert.ToInt16(_netId.ToString("x2"));//Pass _netId to PRB_NW_READ_2W or use (byte)((NetworkNumber/10)* 16 + NetworkNumber%10);

        public short NumberOfIDDigits { get; private set; }

        public short NetworkSize
        {
            get
            {
                if (NetworkNumber == 99)
                {
                    return Convert.ToInt16(NetworkId);
                }
                else
                {
                    return 0;
                }
            }
        }

        public string DialCode {
            get
            {
                // see ETI-16-02 p6
                int readDigit = (_readFlags & 0x0F);
                int digitResolution = (_readFlags & 0xF0)>>4;


                return readDigit.ToString() + digitResolution.ToString();
            }
        }

        public short StopBits {
            get
            {
                 if((_modeFlags & 0x40) > 0)
                {
                    return 1;
                }
                else
                {
                    return 2;
                }
            }
        } // 2,  1



        public short ClockDivisor {
            get
            {
                if ((_modeFlags & 0x80) > 0)
                {
                    return 1;
                }
                else
                {
                    return 16;
                }
            }
        } // 1, 16


        public string WireType {
            get
            {
                if ((_modeFlags & 0x30) == 0x30)
                {
                    return "3W";
                }
                else
                {
                    return "2W";
                }
            }
        } //2W, 3W

        public Char CustChar0 => _custChar0.GetCharFromParity();
        public Char CustChar1 => _custChar1.GetCharFromParity();
        public Char CustChar2 => _custChar2.GetCharFromParity();


        public short ProReadMode
        {
            get
            {
                if((_modeFlags & 0x04) > 0)
                {
                    return 1;
                }
                else
                {
                    return 0;
                }
            }
        }

        public NFPResponseQuery2WOK(byte[] responsePacket) : base(NFPResponseCode.PRB_ANS_OK, responsePacket)
        {

        }



        protected override void ParseParameters()
        {
            _formatCode = _parameters[3];

            
            _networkId1 = _parameters[4];
            _networkId0 = _parameters[5];

            
            _manufacturerId = _parameters[6];

            _registerType = _parameters[7];


            List<byte> etbIndexes = new List<byte>();

            for (int i = 0; i < _parameters.Length; i++)
            {
                if (_parameters[i] == 0x17)
                {
                    etbIndexes.Add((byte)i);
                }
            }

            NumberOfIDDigits = (short)(etbIndexes[2] - etbIndexes[1] - 1);
            _registerNumber = _parameters.Skip(etbIndexes[1] + 1).Take(NumberOfIDDigits).ToArray();


            _custChar0 = _parameters[etbIndexes[3] + 1];
            _custChar1 = _parameters[etbIndexes[3] + 2];
            _custChar2 = _parameters[etbIndexes[3] + 3];
            //_meterId = _parameters.Skip(11).Take(10).ToArray();

            //_packetCheckSum = _parameters.Skip(31).Take(2).ToArray();

            _netId = _parameters[30];

            _readFlags = _parameters[32];

            _modeFlags = _parameters[31];


        }



        public override string ToString()
        {
            if (this.IsPacketValid)
            {

                return $"Response Code: OK, ID Digits: {NumberOfIDDigits}, Network ID: {NetworkId}, Network Number: {NetworkNumber }, Network Size: {NetworkSize}" + Environment.NewLine +
                        $"Manufacturer ID: {ManufacturerId}, Register Type: {RegisterType}, Register Number: {RegisterNumber}" + Environment.NewLine +
                        $"Dial Code: {DialCode}, Stop Bits: {StopBits}, Clock Divisor: {ClockDivisor}, Wire Type: {WireType}, Custom Char: { CustChar0}, { CustChar1}, { CustChar2}" + Environment.NewLine +
                        $"Pro Read Mode: {(ProReadMode == 0 ? "Standard" : "Pro Read Only")}";
            }
            else
            {
                return $"Error: {this.PacketValidationMessage}";
            }
        }


        private byte BuildParametersCheckSum()
        {

            int len = _parameters.Length;
            int result = 0;

            for (int i = 3; i <= 27; i++)
            {
                result += (int)_parameters[i];
            }
            result = result - 123;

            byte b = (byte)(result & 0xFF);

            return b;

        }


        protected override bool IsParametersCheckSumValid()
        {
            //return true;
            _packetCheckSum = _parameters[33];


            byte cs = BuildParametersCheckSum();

            if (cs == _packetCheckSum)
            {
                return true;
            }
            else
            {
                IsPacketValid = false;
                PacketValidationMessage = "Parameters CHECKSUM is incorrect: " + _packetCheckSum.ToString("X2");
                return false;
            }
        }

    }
}
