﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NFPCommunication
{
    public static class ResponseCodeDescription
    {

        private static readonly Dictionary<NFPResponseCode, string> _descriptions = new Dictionary<NFPResponseCode, string>
        {
            { NFPResponseCode.PRB_ANS_OK, "OK" },
            { NFPResponseCode.PRV_ANS_NOK, "Error" },
            { NFPResponseCode.PRB_FRM_ERR_LEN, "Error in received frame length" },
            { NFPResponseCode.PRB_FRM_ERR_CHK,"Error in received frame checksum " },
            { NFPResponseCode.PRB_RES_FRM_TIME_OUT,"Time out error while reading the response frame by the probe" },
            { NFPResponseCode.PRB_DEL_FRM_TIME_OUT,"Time out error that the mouse should wait for a response" },
            { NFPResponseCode.GAS_MIU_NOT_FND,"Gas meter read error" },
            { NFPResponseCode.GAS_PRG_FAIL,"Gas meter programming error" },
            { NFPResponseCode.PRB_DEV_NOT_FOUND, "Device is not ready" },
            { NFPResponseCode.PRB_METER_NOT_READY, "Meter is not connected" },
            { NFPResponseCode.PRB_FRM_ERR_RECV, "Error in responce frame" },
            { NFPResponseCode.APEX_DEV_NOT_FOUND, "APEX Device is not ready" },
            { NFPResponseCode.PRB_QRY_MISMATCH, "Query mismatch" },
            { NFPResponseCode.PRB_MUL_FRM_ERR_RECV,"Multiple reads error" },
            { NFPResponseCode.PRB_INV_CMD, "Invalid Command" }
        };


        public static string GetResponseCodeDescription(NFPResponseCode responceCode)
        {
            return _descriptions[responceCode];
        }
    }
}
