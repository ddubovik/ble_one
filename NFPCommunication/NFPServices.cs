﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace NFPCommunication
{
    public static class NFPServices
    {
        private static readonly IList<NFPService> _services = new List<NFPService>()
        {

            new NFPService(NFPServiceIdentifier.GenericAccess, "Generic Access", new Guid("00001800-0000-1000-8000-00805f9b34fb")),
            new NFPService(NFPServiceIdentifier.PseudoUART,"Pseudo UART", new Guid("4E455054-554E-4554-4749-4E5447490000")),
            new NFPService(NFPServiceIdentifier.ErrorsAndAlerts, "Errors and Alerts", new Guid("792F2E46-4F40-41CB-A7D3-4B8E1DB79489")),
            new NFPService(NFPServiceIdentifier.Battery, "Battery Service", new Guid("0000180f-0000-1000-8000-00805f9b34fb")),
        };

        public static NFPService GetServiceById(NFPServiceIdentifier id)
        {
            return _services.FirstOrDefault(s => s.Id == id);
        }
    }
}
