﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NFPCommunication.Extentions
{
    public static class Extentions
    {

        public static string GetStringFromParity(this byte[] buffer)
        {
            if (buffer == null || buffer.Length == 0)
                return "";

            return Encoding.ASCII.GetString(buffer.Select(b => (byte)(b & 0x7F)).ToArray());
        }


        //TODO: Get rid of this 
        public static string GetStringFromParity(this byte buffer)
        {
            return Encoding.ASCII.GetString(new byte[] { (byte)(buffer & 0x7F) });
        }

        public static char GetCharFromParity (this byte buffer)
        {
            return Encoding.ASCII.GetString(new byte[] { (byte)(buffer & 0x7F) })[0];
        }


        public static byte[] CheckSumAsASCII(this byte buffer)
        {
            string s = (buffer).ToString("X2");

            byte[] b = Encoding.ASCII.GetBytes(s);

            byte b1 = b[0];
            byte b0 = b[1];



            BitArray ba1 = new BitArray(new byte[] { b1 });
            int count = 0;
            for (int i = 0; i < 8; i++)
            {
                if (ba1[i])
                {
                    count++;
                }
            }
            if (count % 2 == 1)
            { 
                ba1[7] = true; 
            }

            count = 0;
            BitArray ba0 = new BitArray(new byte[] { b0 });
            for (int i = 0; i < 8; i++)
            {
                if (ba0[i])
                {
                    count++;
                }
            }
            if (count % 2 == 1)
            { 
                ba0[7] = true; 
            }

            byte[] bytearray0 = new byte[1];
            byte[] bytearray1 = new byte[1];
            ba0.CopyTo(bytearray0, 0);
            ba1.CopyTo(bytearray1, 0);


            return new byte[] { bytearray1[0], bytearray0[0] };
        }


        public static byte BuildParity(this byte buffer)
        {
            BitArray ba1 = new BitArray(new byte[] { buffer });
            int count = 0;
            for (int i = 0; i < 8; i++)
            {
                if (ba1[i])
                {
                    count++;
                }
            }
            if (count % 2 == 1)
            {
                ba1[7] = true;
            }

            byte[] bytearray1 = new byte[1];
            ba1.CopyTo(bytearray1, 0);


            return bytearray1[0];
        }
    }
}
