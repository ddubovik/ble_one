﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NFPCommunication
{
    public class NFPService
    {
        public string Name { get; private set; }

        public NFPServiceIdentifier Id { get; private set; }
        public Guid UUID { get; private set; }

        public NFPService(NFPServiceIdentifier id, string name, Guid uuid)
        {
            Name = name;
            Id = id;
            UUID = uuid;
        }
    }
}
