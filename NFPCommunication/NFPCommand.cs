﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NFPCommunication
{
    public class NFPCommand
    {
        private byte _stx = 0x02;
        private byte _length;
        private NFPCommandCode _functionCode;
        private byte[] _parameters;
        private byte[] _programData;
        private byte _checkSum;
        public byte _etx = 0x03;

        public NFPCommand(NFPCommandCode functionCode, byte[] parameters, byte [] programData)
        {
            _functionCode = functionCode;
            _parameters = parameters;
            _programData = programData;
        }

        public byte[] BuildCommand()
        {
            List<byte> command = new List<byte>();
            int length = 3;
            if (_parameters != null)
            {
                length += _parameters.Length;
            }
            if (_programData != null)
            {
                length += _programData.Length;
            }
            _length = (byte)length;
            command.Add(_stx);
            command.Add(_length);
            command.Add((byte)_functionCode);
            if (_parameters != null)
            {
                command.AddRange(_parameters);
            }
            if (_programData != null)
            {
                command.AddRange(_programData);
            }
            _checkSum = BuildCheckSum();
            command.Add(_checkSum);
            command.Add(_etx);

            //string str = String.Concat(command.ToArray().Select(x => " " + x.ToString("X2")));
            return command.ToArray();
        }

        private byte BuildCheckSum()
        {
            int result = (int)_functionCode + (int)_length;
            if (_parameters != null)
            {
                foreach (byte param in _parameters)
                {
                    result += (int)param;
                }
            }

            if (_programData != null)
            {
                foreach (byte param in _programData)
                {
                    result += (int)param;
                }
            }

            return (byte)(result & 0xFF);
        }
    }
}
