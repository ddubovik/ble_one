﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NFPCommunication
{
    public enum NFPServiceIdentifier
    {
         GenericAccess,
         PseudoUART,
         ErrorsAndAlerts,
         Battery
    }


    public enum NFPCharacteristicIdentifier
    {
        DeviceName,
        BLEAppearance,
        BLEPeripheralPreferredConnectionParameters,
        RX,
        TX,
        ErrorsAndAlerts,
        BatteryStateOfCharge
    }

    public enum NFPCommandCode
    {
        PRB_KEEP_ALV = 0x13,
        PRB_PGM_2W = 0x21,
        PRB_READ_2W = 0x27,
        PRB_READ_3W = 0x28,
        PRB_QRY_2W = 0x24,
        PRB_NWREAD_2W = 0x2A,
        PRB_NWREAD_3W = 0x2B

    }


    public enum NFPResponseCode
    {
        PRB_ANS_OK = 0x90,
        PRV_ANS_NOK = 0x91,
        PRB_FRM_ERR_LEN = 0x92,
        PRB_FRM_ERR_CHK = 0x93,
        PRB_RES_FRM_TIME_OUT = 0x94,
        PRB_DEL_FRM_TIME_OUT = 0x95,
        GAS_MIU_NOT_FND = 0x96,
        GAS_PRG_FAIL = 0x97,
        PRB_DEV_NOT_FOUND = 0x98,
        PRB_METER_NOT_READY = 0x99,
        PRB_FRM_ERR_RECV = 0x9A,
        APEX_DEV_NOT_FOUND = 0x9B,
        PRB_QRY_MISMATCH = 0x9C,
        PRB_MUL_FRM_ERR_RECV = 0x9D,
        PRB_INV_CMD = 0x13

    }


    public enum NFPDataFormat
    {
        BASIC = 0xB1,
        EIGHT_DIGIT = 0xB2
    }
}
