﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NFPCommunication
{
    public class NFPCommandParameters
    {
        public NFPCommandParameters()
        {

        }

        public byte[] GetCommandParameters(short numberOfIDDigits, short numberOfDials, short? network)
        {
            //return new byte[] { 0x4E, 0x20, 0x4E, 0x20, 0x22, 0x02, 0x03 };
            if (network.HasValue)
            {
                return new byte[] { 0x07, 0xD0, 0x07, 0xD0, (byte)(34 - (10 - numberOfIDDigits) - (6 - numberOfDials)), 0x02, 0x03, (byte)network.Value };
            }
            else
            {
                return new byte[] { 0x07, 0xD0, 0x07, 0xD0, (byte)(34 - (10 - numberOfIDDigits) - (6 - numberOfDials)), 0x02, 0x03 };
            }
        }
    }
}
