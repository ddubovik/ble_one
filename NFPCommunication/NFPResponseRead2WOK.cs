﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFPCommunication.Extentions;

namespace NFPCommunication
{
    public class NFPResponseRead2WOK : NFPResponse
    {
        private byte _formatCode;
        public String FormatCode
        {
            get
            {
                if (_formatCode == 0xB1)
                {
                    return "6-Digit";
                }
                if (_formatCode == 0xB2)
                {
                    return "8-Didit";
                }
                return "Unknown";
            }
        }

        private byte _networkId1;
        private byte _networkId0;

        private byte _manufacturerId;

        private byte _meterType;

        private byte[] _meterReadings;


        private byte[] _meterNumber;

        private byte[] _packetCheckSum;

        private byte _custChar0;
        private byte _custChar1;
        private byte _custChar2;

        private byte _numberOfDigitID;

        public string NetworkId => (new byte[] { _networkId1, _networkId0 }).GetStringFromParity();

        public String ManufacturerId => _manufacturerId.GetStringFromParity();

        public String MeterType => _meterType.GetStringFromParity();


        public Char CustChar0 => _custChar0.GetCharFromParity();
        public Char CustChar1 => _custChar1.GetCharFromParity();
        public Char CustChar2 => _custChar2.GetCharFromParity();

        public int NumberOfDigitID => _numberOfDigitID;


        public string MeterId => _meterNumber.GetStringFromParity();

        public string MeterReading
        {
            get
            {
                return _meterReadings.GetStringFromParity();
            }
        }

        public NFPResponseRead2WOK(byte[] responsePacket) : base(NFPResponseCode.PRB_ANS_OK, responsePacket)
        {

        }


        protected override void ParseParameters()
        {
            _formatCode = _parameters[1];

            _networkId1 = _parameters[2];
            _networkId0 = _parameters[3];

            _manufacturerId = _parameters[4];

            _meterType = _parameters[5];


            List<byte> etbIndexes = new List<byte>();

            for(int i = 0; i<_parameters.Length; i ++)
            {
                if (_parameters[i] == 0x17)
                {
                    etbIndexes.Add((byte)i);
                }
            }


            /*
            if (_formatCode == 0xB1)
            {
                _meterReadings = _parameters.Skip(7).Take(6).ToArray(); ;
                //_meterReadings = new byte[] { _parameters[7], _parameters[8], _parameters[9], _parameters[10], _parameters[11], _parameters[12] };
               
            }
            else
            {
                //TODO Test 8-Digit
                _meterReadings = new byte[8];
                
                byte [] reading6 = _parameters.Skip(7).Take(6).ToArray();

                for(int i = 0; i<6; i++)
                {
                    _meterReadings[i] = reading6[i];
                }
                _meterReadings[6] = _parameters[27];
                _meterReadings[7] = _parameters[28];
            }
            */

            _numberOfDigitID = (byte)(etbIndexes[2] - etbIndexes[1] - 1);
            _meterNumber = _parameters.Skip(etbIndexes[1]+1).Take(_numberOfDigitID).ToArray();

            _custChar0 = _parameters[etbIndexes[3] + 1];
            _custChar1 = _parameters[etbIndexes[3] + 2];
            _custChar2 = _parameters[etbIndexes[3] + 3];

            int readingLen = etbIndexes[1] - etbIndexes[0] - 1;
            _meterReadings = _parameters.Skip(etbIndexes[0] + 1).Take(readingLen).ToArray();


            _packetCheckSum = _parameters.Skip(31).Take(2).ToArray();

        }

        public override string ToString()
        {
            if (this.IsPacketValid)
            {
                return  $"Response Code: OK, Number Of Digit ID: {NumberOfDigitID}, Network ID: {NetworkId}, Manufacturer ID: {ManufacturerId}, " +
                        $"Meter Type: {MeterType}," + Environment.NewLine + 
                        $"Meter Reading: {MeterReading}, Meter Number: {MeterId}, " +
                        $"Custom Char: {CustChar0}, {CustChar1}, {CustChar2}";
            }
            else
            {
                return $"Error: {this.PacketValidationMessage}";
            }
        }



        private byte[] BuildParametersCheckSum()
        {

            int len = _parameters.Length;
            int result = 0;

            for (int i = 1; i <= len - 4; i++)
            {
                result += (int)_parameters[i];
            }


            byte b = (byte)(result & 0xFF);

            return b.CheckSumAsASCII();
          
        }


        protected override bool IsParametersCheckSumValid()
        {
            _packetCheckSum = _parameters.Skip(31).Take(2).ToArray();


            byte[] cs = BuildParametersCheckSum();

            if (cs[0] == _packetCheckSum[0] && cs[1] == _packetCheckSum[1])
            {
                return true;
            }
            else
            {
                IsPacketValid = false;
                PacketValidationMessage = "Parameters CHECKSUM is incorrect: " + _packetCheckSum[1].ToString("X2") + " " + _packetCheckSum[0].ToString("X2");
                return false;
            }
        }
    }
}
