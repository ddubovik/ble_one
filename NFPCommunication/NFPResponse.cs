﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NFPCommunication
{
    public class NFPResponse
    {
        private byte _stx = 0x02;
        private byte _length;
        private NFPResponseCode _responseCode;
        protected byte[] _parameters;
        private byte _checkSum;
        public byte _etx = 0x03;
        protected byte[] _responsePacket;
        //protected int _minParametersLength;


        private string _validationMessage;
        public string PacketValidationMessage { get; protected set; }

        public bool IsPacketValid { get; protected set; }


        public string ParametersValidationMessage { get; protected set; }

        public bool AreParametersValid { get; protected set; }

        public NFPResponse(NFPResponseCode _responseCode, byte [] responsePacket)
        {
            _responsePacket = responsePacket;
            //_minParametersLength = minParametersLength;
            if (responsePacket.Length <= 5)
            {
                _parameters = new byte[1];
            }
            _parameters = new byte[responsePacket.Length - 5];

            IsPacketValid = ValidatePacket();

            if (IsPacketValid)
            {
                ParsePacket();

                ParseParameters();

                //if (IsParametersCheckSumValid())
                //{
                //    ParseParameters();
                //}
            }
        }

        private bool ValidatePacket()
        {
            bool result = false;


            //TODO get rid of _minParametersLength.
            // validate Length
            if (_responsePacket.Length <= 5)
            {
                PacketValidationMessage = "Response Packet is too short";
                return result;
            }

            // validate STX
            if (_responsePacket[0] != _stx)
            {
                PacketValidationMessage = "STX is incorrect: " + _responsePacket[0].ToString("X2");
                return result;
            }

            //validate ETX
            if (_responsePacket[_responsePacket.Length -1] != _etx)
            {
                PacketValidationMessage = "ETX is incorrect: " + _responsePacket[_responsePacket.Length - 1].ToString("X2");
                return result;
            }


            _checkSum = BuildCheckSum();


            if (_checkSum != _responsePacket[_responsePacket.Length -2 ])
            {
                PacketValidationMessage = "CHECKSUM is incorrect: " + _responsePacket[_responsePacket.Length - 2].ToString("X2");
                return result;
            }



            result = true;
            return result;
        }


        private void ParsePacket()
        {
            int len = _responsePacket.Length;
            _length = _responsePacket[1];
            _checkSum = _responsePacket[len - 2];

            for (int i=0; i < len - 5 ; i++)
            {
                _parameters[i] = _responsePacket[i + 3];
            }
        }

        private byte BuildCheckSum()
        {

            int len = _responsePacket.Length;
            int result = 0;

            for (int i = 1; i <= len - 3; i ++)
            {
                result += (int)_responsePacket[i];
            }
            return (byte)(result & 0xFF);
        }


        protected virtual void ParseParameters()
        {

        }

        protected virtual bool IsParametersCheckSumValid()
        {
            return true;
        }
       

      
        
    }
}
