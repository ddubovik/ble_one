﻿using Plugin.BluetoothLE;
using System;
using System.Collections.Generic;
using System.Text;

namespace BLE_One.Models
{
    class BLEDevice
    {
        public IDevice Device { get;  set; }
        public string Name { get; set; }
    }
}
