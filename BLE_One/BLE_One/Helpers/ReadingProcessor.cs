﻿using System;
using System.Collections.Generic;
using System.Text;
using static BLE_One.EnumUtility;
using System.Linq;
using NFPCommunication;

namespace BLE_One
{
    public class ReadingProcessor
    {
        public ReadingProcessor()
        {
            State = ReadingState.Idle;
        }

        private object _lock = new object();
        public ReadingState State { get; set; }


        public string ReadingAsHexString()
        {
            return String.Concat(ReadingBuffer.Take(_bytesRead).Select(x => " " + x.ToString("X2")));
        }


        public byte[] ReadingAsBytes()
        {
            return ReadingBuffer.Take(_bytesRead).ToArray();
        }


        public byte[] ReadingBuffer = new byte[1024];

        private int _bytesRead = 0;

        public int CommandSize = 0;

        public NFPCommandCode CommandCode = 0;

        public bool AddNewReading (byte[] NewReading)
        {
            //System.Diagnostics.Debug.WriteLine("AddNewReading() begin. " + DateTime.Now.ToString("MM/dd/yyyy hh:mm:ss.fff"));
            lock (_lock)
            {
                if (State == ReadingState.ReadingComplete)
                {
                    return true;
                }
                if (State == ReadingState.Idle)
                {
                    State = ReadingState.ReadingInProgress;
                }
                if ((_bytesRead + NewReading.Length) > ReadingBuffer.Length)
                {
                    State = ReadingState.ReadingError;
                    return false;
                }
                else
                {
                    for (int i = 0; i < NewReading.Length; i++)
                    {
                        ReadingBuffer[_bytesRead] = NewReading[i];
                        _bytesRead++;
                    }
                    //System.Diagnostics.Debug.WriteLine("AddNewReading() end. " + DateTime.Now.ToString("MM/dd/yyyy hh:mm:ss.fff"));
                    
                    return true;
                }
            }
            
        }

        public void PurgeBuffer()
        {
            for (int i= 0; i<_bytesRead; i++)
            {
                ReadingBuffer[i] = 0;
            }
            State = ReadingState.Idle;
            _bytesRead = 0;

        }


        public void NewCommand( NFPCommandCode command)
        {
            CommandCode = command;
            PurgeBuffer();
        }


        public void EvaluateState()
        {
            //System.Diagnostics.Debug.WriteLine("EvaluateState() begin. " + DateTime.Now.ToString("MM/dd/yyyy hh:mm:ss.fff"));
            lock (_lock)
            {
                if (State == ReadingState.ReadingInProgress)
                {
                    if (_bytesRead > 1)
                    {
                        CommandSize = ReadingBuffer[1];
                        

                        if (_bytesRead == CommandSize + 2)
                        {
                            if (ReadingBuffer[_bytesRead - 1] == 3)
                            {
                                State = ReadingState.ReadingComplete;
                            }
                            else
                            {
                                State = ReadingState.ReadingError;
                            }
                        }
                    }
                }
            }
            //System.Diagnostics.Debug.WriteLine("EvaluateState() end. " + DateTime.Now.ToString("MM/dd/yyyy hh:mm:ss.fff"));
        }




    }
}
