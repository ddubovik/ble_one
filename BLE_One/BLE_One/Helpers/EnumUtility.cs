﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BLE_One
{
    public static class EnumUtility
    {
        public  enum ReadingState
        {
            Idle,
            ReadingInProgress,
            ReadingComplete,
            ReadingError
        }
    }
}
