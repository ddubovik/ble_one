﻿using Plugin.BluetoothLE;
using System;
using System.Collections.Generic;
using System.Reactive.Linq;
using System.Text;
using System.Threading;
using Xamarin.Forms;
using NFPCommunication;
using System.Linq;

namespace BLE_One.ViewModels
{
    class DeviceDetailViewModel : BaseViewModel
    {

        public ReadingProcessor rP;

        public IDevice device;

        IGattService NFPGattService;

        IGattCharacteristic BatteryCharacteristic;

        IGattCharacteristic RxWriteCharacteristic;
        IDisposable RxWriteCharacteristicSubscription;

        IGattCharacteristic TxReadCharacteristic;
        IDisposable TxReadCharacteristicSubscription;

        private string _connectText = "Connecting";
        public string ConnectText {
            get
            {
                return _connectText;
            }
            private set
            {
                _connectText = value;
                OnPropertyChanged("ConnectText");
            }
        }



        private bool _isNFPService = false;
        public bool IsNFPService
        {
            get
            {
                return _isNFPService;
            }
            set
            {
                _isNFPService = value;
                OnPropertyChanged("IsNFPService");
            }
        }


        private string _deviceName;
        public string DeviceName
        {
            get
            {
                return _deviceName;
            }
            set
            {
                _deviceName = value;
                OnPropertyChanged("DeviceName");
            }
        }

        private int? _batteryLevel;

        public string BatteryLevel {
            get
            {
                return _batteryLevel.HasValue? $"{_batteryLevel} %" : "N/A";
            }
        }


        public string _KeepAliveHexResponse = "N/A";

        public string KeepAliveHexResponse
        {
            get
            {
                return _KeepAliveHexResponse;
            }
            set
            {
                _KeepAliveHexResponse = value;
                OnPropertyChanged("KeepAliveHexResponse");
            }
        }


        public string _keepAliveStringResponse = "N/A";

        public string KeepAliveStringResponse
        {
            get
            {
                return _keepAliveStringResponse;
            }
            set
            {
                _keepAliveStringResponse = value;
                OnPropertyChanged("KeepAliveStringResponse");
            }
        }

        public string _read2WStringResponse = "N/A";

        public string Read2WStringResponse
        {
            get
            {
                return _read2WStringResponse;
            }
            set
            {
                _read2WStringResponse = value;
                OnPropertyChanged("Read2WStringResponse");
            }
        }

        public string _Read2WResponse = "N/A";

        public string Read2WResponse
        {
            get
            {
                return _Read2WResponse;
            }
            set
            {
                _Read2WResponse = value;
                OnPropertyChanged("Read2WResponse");
            }
        }



        public string _Read3WResponse = "N/A";

        public string Read3WResponse
        {
            get
            {
                return _Read3WResponse;
            }
            set
            {
                _Read3WResponse = value;
                OnPropertyChanged("Read3WResponse");
            }
        }


        public string _read3WStringResponse = "N/A";

        public string Read3WStringResponse
        {
            get
            {
                return _read3WStringResponse;
            }
            set
            {
                _read3WStringResponse = value;
                OnPropertyChanged("Read3WStringResponse");
            }
        }


        public string _Query2WResponse = "N/A";

        public string Query2WResponse
        {
            get
            {
                return _Query2WResponse;
            }
            set
            {
                _Query2WResponse = value;
                OnPropertyChanged("Query2WResponse");
            }
        }

        public string _query2WStringResponse = "N/A";

        public string Query2WStringResponse
        {
            get
            {
                return _query2WStringResponse;
            }
            set
            {
                _query2WStringResponse = value;
                OnPropertyChanged("Query2WStringResponse");
            }
        }



        public string _program2WResponse = "N/A";

        public string Program2WResponse
        {
            get
            {
                return _program2WResponse;
            }
            set
            {
                _program2WResponse = value;
                OnPropertyChanged("Program2WResponse");
            }
        }

        public string _program2WStringResponse = "N/A";

        public string Program2WStringResponse
        {
            get
            {
                return _program2WStringResponse;
            }
            set
            {
                _program2WStringResponse = value;
                OnPropertyChanged("Program2WStringResponse");
            }
        }



        public string _readNw2WResponse = "N/A";

        public string ReadNw2WResponse
        {
            get
            {
                return _readNw2WResponse;
            }
            set
            {
                _readNw2WResponse = value;
                OnPropertyChanged("ReadNw2WResponse");
            }
        }

        public string _readNw2WStringResponse = "N/A";

        public string ReadNw2WStringResponse
        {
            get
            {
                return _readNw2WStringResponse;
            }
            set
            {
                _readNw2WStringResponse = value;
                OnPropertyChanged("ReadNw2WStringResponse");
            }
        }

        public string _readNw3WResponse = "N/A";

        public string ReadNw3WResponse
        {
            get
            {
                return _readNw3WResponse;
            }
            set
            {
                _readNw3WResponse = value;
                OnPropertyChanged("ReadNw3WResponse");
            }
        }

        public string _readNw3WStringResponse = "N/A";

        public string ReadNw3WStringResponse
        {
            get
            {
                return _readNw3WStringResponse;
            }
            set
            {
                _readNw3WStringResponse = value;
                OnPropertyChanged("ReadNw3WStringResponse");
            }
        }

        private string _meterID = "1234567890";

        public string MeterID
        {
            get
            {
                return _meterID;
            }
            set
            {
                _meterID = value;
                OnPropertyChanged("MeterID");
            }
        }

        private string _dataFormat = "10-Digit";

        public string DataFormat
        {
            get
            {
                return _dataFormat;
            }
            set
            {
                _dataFormat = value;
                OnPropertyChanged("DataFormat");
            }
        }

        public List<string> DataFormats { get; } = new List<string> { "6-Digit", "8-Digit", "10-Digit" };

        private string _dialCode = "65";
        public string DialCode { get
            {
                return _dialCode;
            }
            set
            {
                _dialCode = value;
                OnPropertyChanged("DialCode");
            }
        }

        public List<string> DialCodes { get; } = new List<string> { "49","59", "65" };

        public Command CmdConnectionToggle { get; set; }

        public Command CmdKeepAlive { get; set; }

        public Command CmdRead2W { get; set; }


        public Command CmdRead3W { get; set; }


        public Command CmdQuery2W { get; set; }

        public Command CmdProgram2W { get; set; }

        public Command CmdReadNw2W { get; set; }

        public Command CmdReadNw3W { get; set; }


        public DeviceDetailViewModel(IDevice device)
        {

            rP = new ReadingProcessor();
            this.device = device;
            DeviceName = device.Name;

            if (device.Status == ConnectionStatus.Connected)
            {
                ConnectText = "Disconnect";
            }
            else
            {
                device.Connect();
                ConnectText = "Connecting";
            }
            

            CmdConnectionToggle = new Command(() =>
            {
                ConnectToggle();

            });

            CmdKeepAlive = new Command(() =>
            {
                KeepAlive();

            });


            CmdRead2W = new Command(() =>
            {
                Read2W();

            });


            CmdRead3W = new Command(() =>
            {
                Read3W();

            });

            CmdQuery2W = new Command(() =>
            {
                Query2W();

            });


            CmdProgram2W = new Command(() =>
            {
                Program2W();

            });

            CmdReadNw3W = new Command(() =>
            {
                ReadNw3W();

            });

            CmdReadNw2W = new Command(() =>
            {
                ReadNw2W();

            });


            this.device
               .WhenStatusChanged()
               .Subscribe(status =>
               {
                   switch (status)
                   {
                       case ConnectionStatus.Connecting:
                           this.ConnectText = "Connecting";
                           break;

                       case ConnectionStatus.Connected:
                           this.ConnectText = "Disconnect";

                       
                           
                           this.device.GetKnownService(NFPServices.GetServiceById(NFPServiceIdentifier.PseudoUART).UUID) //TODO try WhenConnectedGetKnownService
                          .Subscribe(gattService =>
                          {
                              NFPGattService = gattService;
                              IsNFPService = true;

                          });
                           
                           this.device.GetKnownCharacteristics(
                               NFPServices.GetServiceById(NFPServiceIdentifier.Battery).UUID, 
                               NFPCharacteristics.GetServiceById(NFPCharacteristicIdentifier.BatteryStateOfCharge).UUID)
                           .Subscribe(gattCharacteristic => {
                               BatteryCharacteristic = gattCharacteristic;
                               BatteryCharacteristic.Read().Timeout(TimeSpan.FromSeconds(5))
                                    .Subscribe(
                                        x => ProcessBattery(x),
                                        ex => HandleError(ex.ToString())
                                    );},
                   ex=> HandleError(ex.ToString()));
                   


                          

                         RxWriteCharacteristicSubscription =   this.device.GetKnownCharacteristics(
                                NFPServices.GetServiceById(NFPServiceIdentifier.PseudoUART).UUID,
                                NFPCharacteristics.GetServiceById(NFPCharacteristicIdentifier.RX).UUID)
                          .Subscribe(gattCharacteristic => {
                              RxWriteCharacteristic = gattCharacteristic;

                          });

                           this.device.GetKnownCharacteristics(
                                NFPServices.GetServiceById(NFPServiceIdentifier.PseudoUART).UUID,
                                NFPCharacteristics.GetServiceById(NFPCharacteristicIdentifier.TX).UUID)
                         .Subscribe(gattCharacteristic => {
                             TxReadCharacteristic = gattCharacteristic;
                             /*
                             TxReadCharacteristic.EnableNotifications();
                             TxReadCharacteristic.DiscoverDescriptors()
                             .Subscribe(DescriptorGattResult => {
                                 
                                 string descriptor = DescriptorGattResult.Uuid.ToString();
                                 DescriptorGattResult.Write(new byte[] { 0x02, 0x00 })
                                 .Subscribe( DescriptorGattResult2=> {
                                     int c = 5;
                                 });
                                
                             });
                             */

                             TxReadCharacteristicSubscription = TxReadCharacteristic.RegisterAndNotify().Subscribe(
                                 characteristicGattResult => ProcessKeepAliveResponse(characteristicGattResult));
                                 
                             /*
                             TxReadCharacteristic.WhenNotificationReceived()
                             .Subscribe(
                                 characteristicGattResult => ProcessKeepAliveResponse(characteristicGattResult));
                                 */
                         });
                           break;

                       case ConnectionStatus.Disconnected:
                           this.ConnectText = "Connect";
                           IsNFPService = false;

                           break;
                   }
               });


            //this.device.GetKnownService(new Guid("4E455054-554E-4554-4749-4E5447490000"))
            //    .Subscribe(gattService => 
            //    {
            //        int c = 5;
            //    });

        }


        public void ConnectToggle()
        {
            if (this.device.Status == ConnectionStatus.Disconnected)
            {
                this.device.Connect();
            }
            else
            {
                this.device.CancelConnection();
                //this.RxWriteCharacteristicSubscription.Dispose();
                this.TxReadCharacteristicSubscription.Dispose();
               // this.RxWriteCharacteristic = null;
                //this.RxWriteCharacteristic = null;
            }
        }



        public void KeepAlive()
        {
            KeepAliveHexResponse = "Processing";
            KeepAliveStringResponse = "Processing";

            rP.NewCommand(NFPCommandCode.PRB_KEEP_ALV);
            //byte[] rawCommand = { 2, 3, 19, 22, 3 }; //"0203131603h"
            //NFPCommand cmd = new NFPCommand(NFPCommandCode.PRB_KEEP_ALV);

            NFPCommand cmd = NFPCommandFactory.GetCommand(NFPCommandCode.PRB_KEEP_ALV, null, null);
            byte[] rawCommand = cmd.BuildCommand();
            this.RxWriteCharacteristic.Write(rawCommand)
                .Subscribe(CharacteristicGattResult =>
                {

                });

            /*
            NFCProgramData pd = new NFCProgramData();
            pd.MeterNumber = 987654321;
            byte[] mn =  pd.BiuldProgramData();

            string s = String.Concat(mn.Select(x => " " + x.ToString("X2")));


            NFPCommand pc = NFPCommandFactory.GetCommand(NFPCommandCode.PRB_PGM_2W, mn);
            byte[] pcBytes = pc.BuildCommand();
            string spc = String.Concat(pcBytes.Select(x => " " + x.ToString("X2")));
            */
            int c = 5;

        }


        public void Read2W()
        {
            Read2WResponse = "Processing";
            Read2WStringResponse = "Processing";

            rP.NewCommand(NFPCommandCode.PRB_READ_2W);
            //byte[] rawCommand = { 0x02, 0x0A, 0x27, 0x4E, 0x20, 0x4E, 0x20,0x1E, 0x02, 0x00, 0x2D, 0x03 }; //"02 10 39 78 32 78 30 02 00 45 03"
            // byte[] parameters = { 0x4E, 0x20, 0x4E, 0x20, 0x1E, 0x02, 0x00 };
            //NFPCommand cmd = new NFPCommand(NFPCommandCode.PRB_READ_2W, parameters);

            NFPCommand cmd = NFPCommandFactory.GetCommand(NFPCommandCode.PRB_READ_2W,null,null);
            byte[] rawCommand = cmd.BuildCommand();
            this.RxWriteCharacteristic.Write(rawCommand)

                .Subscribe(CharacteristicGattResult => {
                });
        }


        public void Read3W()
        {
            Read3WResponse = "Processing";
            Read3WStringResponse = "Processing";

            rP.NewCommand(NFPCommandCode.PRB_READ_3W);
            NFPCommand cmd = NFPCommandFactory.GetCommand(NFPCommandCode.PRB_READ_3W, null, null);
            byte[] rawCommand = cmd.BuildCommand();
            this.RxWriteCharacteristic.Write(rawCommand)

                .Subscribe(CharacteristicGattResult => {
                   
                });
        }


        public void Query2W()
        {
            Query2WResponse = "Processing";
            Query2WStringResponse = "Processing";

            rP.NewCommand(NFPCommandCode.PRB_QRY_2W);
            NFPCommand cmd = NFPCommandFactory.GetCommand(NFPCommandCode.PRB_QRY_2W, null, null);
            byte[] rawCommand = cmd.BuildCommand();
            this.RxWriteCharacteristic.Write(rawCommand)

                .Subscribe(CharacteristicGattResult => {

                });
        }



        public void Program2W()
        {
            Program2WResponse = "Processing";
            Program2WStringResponse = "Processing";

            NFPProgramData pd = new NFPProgramData();
            pd.RegisterNumber = Convert.ToInt64( MeterID);

            if (DataFormat == "6-Digit")
            {
                pd.MeterNumberDigits = 6;
            }
            if (DataFormat == "8-Digit")
            {
                pd.MeterNumberDigits = 8;
            }
            pd.DialCode = DialCode;
            pd.ClockDivisor = 16;
            pd.StopBits = 2;
            pd.WireType = "3W";
            pd.NetworkNumber = 0;
            pd.NetworkSize = 0;
            pd.ProReadMode = 0;
            pd.CustChar0 = 'A';
            pd.CustChar1 = 'B';
            pd.CustChar2 = 'C';
            byte[] mn = pd.BiuldProgramData();

            rP.NewCommand(NFPCommandCode.PRB_PGM_2W);
            NFPCommand cmd = NFPCommandFactory.GetCommand(NFPCommandCode.PRB_PGM_2W, mn, null);
            byte[] rawCommand = cmd.BuildCommand();
            this.RxWriteCharacteristic.Write(rawCommand)

                .Subscribe(CharacteristicGattResult => {

                });



        }

        public void ReadNw2W()
        {
            ReadNw2WResponse = "Processing";
            ReadNw2WStringResponse = "Processing";

            rP.NewCommand(NFPCommandCode.PRB_NWREAD_2W);
            NFPCommand cmd = NFPCommandFactory.GetCommand(NFPCommandCode.PRB_NWREAD_2W, null, 1);
            byte[] rawCommand = cmd.BuildCommand();
            this.RxWriteCharacteristic.Write(rawCommand)
                .Subscribe(CharacteristicGattResult => {
                });
        }

        public void ReadNw3W()
        {
            ReadNw3WResponse = "Processing";
            ReadNw3WStringResponse = "Processing";

            rP.NewCommand(NFPCommandCode.PRB_NWREAD_3W);
            NFPCommand cmd = NFPCommandFactory.GetCommand(NFPCommandCode.PRB_NWREAD_3W, null, 1);
            byte[] rawCommand = cmd.BuildCommand();
            this.RxWriteCharacteristic.Write(rawCommand)
                .Subscribe(CharacteristicGattResult => {
                });
        }


        private void ProcessKeepAliveResponse(CharacteristicGattResult result)
        {
            //Thread.Sleep(50);
            //KeepAliveHexResponse = "Processing";

            if (rP.State == EnumUtility.ReadingState.Idle)
            {

                switch (rP.CommandCode)
                {
                    case NFPCommandCode.PRB_KEEP_ALV:
                        KeepAliveHexResponse = "Processing";
                        KeepAliveStringResponse = "Processing";
                        break;
                    case NFPCommandCode.PRB_READ_2W:
                        Read2WResponse = "Processing";
                        Read2WStringResponse = "Processing";
                        break;
                    case NFPCommandCode.PRB_READ_3W:
                        Read3WResponse = "Processing";
                        Read3WStringResponse = "Processing";
                        break;
                    case NFPCommandCode.PRB_QRY_2W:
                        Query2WResponse = "Processing";
                        Query2WStringResponse = "Processing";
                        break;
                    case NFPCommandCode.PRB_PGM_2W:
                        Program2WResponse = "Processing";
                        Program2WStringResponse = "Processing";
                        break;
                    case NFPCommandCode.PRB_NWREAD_2W:
                        ReadNw2WResponse = "Processing";
                        ReadNw2WStringResponse = "Processing";
                        break;
                    case NFPCommandCode.PRB_NWREAD_3W:
                        ReadNw3WResponse = "Processing";
                        ReadNw3WStringResponse = "Processing";
                        break;
                }
               
            }
            rP.AddNewReading(result.Data);
            //System.Diagnostics.Debug.WriteLine(DateTime.Now.ToString("MM/dd/yyyy hh:mm:ss.fff"));
            rP.EvaluateState();
            if (rP.State == EnumUtility.ReadingState.ReadingComplete)
            {
                NFPResponse response;
                switch (rP.CommandCode)
                {
                    case NFPCommandCode.PRB_KEEP_ALV:
                        KeepAliveHexResponse = "Complete: " + rP.ReadingAsHexString();
                        response = NFPResponseFactory.GetResponse(NFPCommandCode.PRB_KEEP_ALV, rP.ReadingAsBytes());
                        KeepAliveStringResponse = response.ToString();
                        break;
                    case NFPCommandCode.PRB_READ_2W:
                        Read2WResponse = "Complete: " + rP.ReadingAsHexString();
                        response = NFPResponseFactory.GetResponse(NFPCommandCode.PRB_READ_2W, rP.ReadingAsBytes());
                        Read2WStringResponse = response.ToString();
                        break;
                    case NFPCommandCode.PRB_READ_3W:
                        Read3WResponse = "Complete: " + rP.ReadingAsHexString();
                        response = NFPResponseFactory.GetResponse(NFPCommandCode.PRB_READ_3W, rP.ReadingAsBytes());
                        Read3WStringResponse = response.ToString();
                        break;
                    case NFPCommandCode.PRB_QRY_2W:
                        Query2WResponse = "Complete: " + rP.ReadingAsHexString();
                        response = NFPResponseFactory.GetResponse(NFPCommandCode.PRB_QRY_2W, rP.ReadingAsBytes());
                        Query2WStringResponse = response.ToString();
                        break;
                    case NFPCommandCode.PRB_PGM_2W:
                        Program2WResponse = "Complete: " + rP.ReadingAsHexString();
                        response = NFPResponseFactory.GetResponse(NFPCommandCode.PRB_PGM_2W, rP.ReadingAsBytes());
                        Program2WStringResponse = response.ToString();
                        break;
                    case NFPCommandCode.PRB_NWREAD_2W:
                        ReadNw2WResponse = "Complete: " + rP.ReadingAsHexString();
                        response = NFPResponseFactory.GetResponse(NFPCommandCode.PRB_NWREAD_2W, rP.ReadingAsBytes());
                        ReadNw2WStringResponse = response.ToString();
                        break;
                    case NFPCommandCode.PRB_NWREAD_3W:
                        ReadNw3WResponse = "Complete: " + rP.ReadingAsHexString();
                        response = NFPResponseFactory.GetResponse(NFPCommandCode.PRB_NWREAD_3W, rP.ReadingAsBytes());
                        ReadNw3WStringResponse = response.ToString();
                        break;
                }
            }
        }

        private void ProcessBattery(CharacteristicGattResult result)
        {
            if (result.Data == null)
            {
                _batteryLevel = null;
            }
            else
            {
                _batteryLevel = result.Data[0];
            }
            OnPropertyChanged("BatteryLevel");
        }
        private void HandleError(string error)
        {
            int c = 5;
        }



      
    }
}
