﻿using BLE_One.Models;
using Plugin.BluetoothLE;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;

namespace BLE_One.ViewModels
{
    class DevicesViewModel : BaseViewModel
    {

        IAdapter adapter;
        IDisposable scan;
        //public ICommand ScanToggle { get; }

        private bool _isScaning = false; 
        public bool IsScanning {
            get
            {
                return _isScaning;
            }
            set
            {
                _isScaning = value;
                OnPropertyChanged("IsScanning");
            }
        }




        public Command Scan { get; set; }


        //private BLEDevice _selectedDevice;


        //public BLEDevice SelectedDevice
        //{
        //    get
        //    {
        //        return _selectedDevice;
        //    }
        //    set
        //    {
        //        _selectedDevice = value;

        //        if (_selectedDevice == null)
        //            return;
        //        MainP

        //        _selectedDevice = null;
        //    }
        //}

        public ObservableCollection<BLEDevice> Devices { get; set; }

        public DevicesViewModel()
        {
            Title = "Devices";
            /*
            var scanner = CrossBleAdapter.Current.Scan().Subscribe(scanResult =>
            {
                if (!String.IsNullOrEmpty(scanResult.Device.Name))
                {
                    Devices.Add(new BLEDevice() { Name = scanResult.Device.Name});
                }

            });*/
            Devices = new ObservableCollection<BLEDevice>();

            Scan = new Command(() =>
            {
                if (IsScanning)
                {
                    this.scan?.Dispose();
                    IsScanning = false;
                }
                else
                {
                    IsScanning = true;
                    scan = CrossBleAdapter.Current.Scan().Subscribe(scanResult =>
                    {
                        if (!String.IsNullOrEmpty(scanResult.Device.Name))
                        {
                            if (!Devices.Any(d => d.Name == scanResult.Device.Name))
                            {
                                Devices.Add(new BLEDevice() { Name = scanResult.Device.Name, Device = scanResult.Device });
                            }
                        }

                    });
                }

            });


            






        }




    }
}
