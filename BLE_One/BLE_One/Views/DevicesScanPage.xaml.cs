﻿using BLE_One.Models;
using BLE_One.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace BLE_One.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class DevicesScanPage : ContentPage
    {
        public DevicesScanPage()
        {
            InitializeComponent();
            BindingContext = new DevicesViewModel();
        }



        async void OnDeviceSelected(object sender, SelectedItemChangedEventArgs args)
        {
            var item = args.SelectedItem as BLEDevice;
            if (item == null)
                return;

            await Navigation.PushAsync(new DeviceDetailPage(item.Device));

            // Manually deselect item.
            ItemsListView.SelectedItem = null;
        }
    }
}