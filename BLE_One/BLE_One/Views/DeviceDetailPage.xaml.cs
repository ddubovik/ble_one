﻿using BLE_One.ViewModels;
using Plugin.BluetoothLE;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace BLE_One.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class DeviceDetailPage : ContentPage
	{
		public DeviceDetailPage (IDevice device)
		{
			InitializeComponent();

            this.BindingContext = new DeviceDetailViewModel(device);

        }

        

	}
}