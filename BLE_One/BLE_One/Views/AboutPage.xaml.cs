﻿using System;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace BLE_One.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AboutPage : ContentPage
    {
        public AboutPage()
        {
            InitializeComponent();
        }
    }
}